# Docker Files for Vue FE with Py-Flask API and Postgres BE

This repository is an example of how you can deploy Docker Image Containers housing a frontend and backend application "set" and "run" the code images directly from their "started" Docker containers.  (note that this project is an example initial deployment.  The images and containers have not been perfected for security, image instance version creep and performance).

Included are the DockerFiles and Docker Composer Files for deploying the Client Server based "set" of application services.  The setup mimics micro services with 3 respective application services running in their own respective Docker containers (frontend, backend and db).  The frontend is a vue application, the backend is a python/flask api and the db is a postgres database.

Two Docker Images have been uploaded into the project's GitLab container registry (Github and Docker have restrictions on registries)

**Docker and Docker Compose are required** (install Docker to your local workstation, os instance, etc.; see [docker.com](url) )
The db image will be pulled directly from Docker Hub.

Download the zip file of this repository or clone this repostitory, it will ...

Create two sub directories inside your parent project directory (example: /devops-exercise/devops-exercise-frontend-main and /devops-exercise/devops-exercise-backend-main)

Pull two Docker Images from GitLab
   - docker pull registry.gitlab.com/david402/docker-files-for-vue-fe-with-py-flask-api-and-postgres-be/devopsexercisebackendmain:v1
   - docker pull registry.gitlab.com/david402/docker-files-for-vue-fe-with-py-flask-api-and-postgres-be/devopsexercisefrontendmain:v1

Use the frontend and backend source files respectively from: https://gitlab.com/campusedu/public/devops-exercise-main

Run docker-compose in each sub directory, the backend process will pull its dependent db image from Docker Hub.
   - docker-compose up -d (from devops-exercise-frontend-main)
      this will start the vue app in a container
   - docker-compose up -d (from devops-exercise-backend-main)
      this will start the python/flask api in a 2nd container and start postgres in a 3rd container


From your browser, run http://localhost:8080





